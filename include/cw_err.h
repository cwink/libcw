#ifndef CW_ERR_H
#define CW_ERR_H

enum cw_err {
	CW_ERR_NONE = 0,
	CW_ERR_SYS,
	CW_ERR_MAX,
};

const char *cw_err_str(enum cw_err err);

#endif /* CW_ERR_H */
