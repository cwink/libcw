#ifndef CW_BOOL_H
#define CW_BOOL_H

enum cw_bool {
	CW_BOOL_FALSE = 0,
	CW_BOOL_TRUE,
	CW_BOOL_MAX,
};

#endif /* CW_BOOL_H */
