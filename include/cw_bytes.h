#ifndef CW_BYTES_H
#define CW_BYTES_H

#define cw_bytes_from_kbs(kbs) (kbs * 1000)

#define cw_bytes_from_mbs(mbs) (mbs * 1000000)

#endif /* CW_BYTES_H */
