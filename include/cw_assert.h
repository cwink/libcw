#ifndef CW_ASSERT_H
#define CW_ASSERT_H

#ifdef CW_DEBUG
	#if defined(__GNUC__)
		#define cw_assert(cond) if (!(cond)) __builtin_trap()
	#elif defined(_MSC_VER)
		#define cw_assert(cond) if (!(cond)) __debugbreak()
	#else
		#define cw_assert(cond) if (!(cond)) *(volatile int *)0 = 0
	#endif
#else
	#define cw_assert(cond)
#endif /* CW_DEBUG */

#endif /* CW_ASSERT_H */
