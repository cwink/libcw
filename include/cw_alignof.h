#ifndef CW_ALIGNOF_H
#define CW_ALIGNOF_H

#if defined(__GNUC__)
	#define cw_alignof(type) __alignof__(type)
#elif defined(_MSC_VER)
	#define cw_alignof(type) __alignof(type)
#else
	#error Unable to detect an alignof function.
#endif

#endif /* CW_ALIGNOF_H */
