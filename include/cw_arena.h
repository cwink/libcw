#include <stddef.h>
#include <stdint.h>

#include "cw_bool.h"
#include "cw_err.h"

struct cw_arena {
	uint8_t *beg, *end, *ptr;
	enum cw_bool open;
};

void *		cw_arena_alloc(struct cw_arena *arena, size_t size, size_t len, size_t align);
enum cw_err	cw_arena_close(struct cw_arena *arena);
void		cw_arena_init(struct cw_arena *arena);
enum cw_err	cw_arena_open(struct cw_arena *arena, size_t size);
