#!/bin/sh

ar="ar"
arflags="rcs"
cc="gcc"
cflags="-Iinclude -std=c99 -xc"
cflags_deb="-DCW_DEBUG -fsanitize=undefined -fsanitize-trap -g3 -O0 -pedantic-errors -Wall -Wconversion -Wdouble-promotion -Wextra"
cflags_rel="-Os"
debug=1
ldflags="-Lbuild"
ldflags_deb=""
ldflags_rel="-s -Wl,--gc-sections"
libs="-lcw"
lint_flags="-D__GNUC__ -DCW_DEBUG --enable=all -Iinclude --language=c --suppress=missingIncludeSystem --std=c99"
linter="cppcheck"
platform="posix"
tagger="ctags"
tags_flags="-aR --fields=+S --kinds-c=+p"
tags_inc="app src include ${HOME}/.local/src/libX11/src/libX11-1.8.7/src /usr/include/X11 /usr/include/GL"

find_apps()
{
	find "app" -type f -path "app/*.c"
}

find_srcs()
{
	find "src" ! -path "src/*/*" -type f -path "src/*.c"
	find "src" -type f -path "src/${platform}/*.c"
}

gen_config()
{
	cat <<EOF
AR         = ${ar}
ARFLAGS    = ${arflags}
CC         = ${cc}
CFLAGS     = ${cflags}
LDFLAGS    = ${ldflags}
LIBS       = ${libs}
LINT_FLAGS = ${lint_flags}
LINTER     = ${linter}
TAGGER     = ${tagger}
TAGS_FLAGS = ${tags_flags}
TAGS_INC   = ${tags_inc}
EOF
}

gen_source()
{
	exes=""
	objs=""

	for app in $(find_apps); do
		name="$(basename "${app}" | sed "s/\.c$//")"
		exes="${exes} build/${name}.x"
	done
	cat <<EOF
all: build/libcw.a${exes}
EOF
	for app in $(find_apps); do
		name="$(basename "${app}" | sed "s/\.c$//")"
		cat <<EOF

build/${name}.x: build/${name}.o
	\$(CC) \$(LDFLAGS) -o \$(@) \$(^) \$(LIBS)

build/${name}.o: ${app}
	\$(CC) \$(CFLAGS) -c -o \$(@) \$(<)
EOF
	done
	for src in $(find_srcs); do
		name="$(basename "${src}" | sed "s/\.c$//")"
		objs="${objs} build/${name}.o"
		cat <<EOF

build/${name}.o: ${src}
	\$(CC) \$(CFLAGS) -c -o \$(@) \$(<)
EOF
	done
	cat <<EOF

build/libcw.a:${objs}
	\$(AR) \$(ARFLAGS) \$(@) \$(^)
EOF
}

if [ "${debug}" -eq 1 ]; then
	cflags="${cflags} ${cflags_deb}"
	ldflags="${ldflags} ${ldflags_deb}"
else
	cflags="${cflags} ${cflags_rel}"
	ldflags="${ldflags} ${ldflags_rel}"
fi

gen_config >"config.mk"
gen_source >"source.mk"
