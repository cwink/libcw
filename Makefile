.PHONY: all clean lint tags
.POSIX:
.SUFFIXES:

include config.mk
include source.mk

clean:
	rm -f build/*.a build/*.o build/*.x tags

lint:
	$(LINTER) $(LINT_FLAGS) .

tags:
	$(TAGGER) $(TAGS_FLAGS) $(TAGS_INC)
