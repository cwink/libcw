#include "cw_arena.h"
#include "cw_assert.h"

void *
cw_arena_alloc(struct cw_arena *const arena, const size_t size, const size_t len, const size_t align)
{
	cw_assert(arena->open == CW_BOOL_TRUE && size > 0 && len > 0 && align > 0);
	arena->ptr = (uint8_t *)(((uintptr_t)arena->ptr + (align - 1)) & ~(align - 1)) + (size * len);
	cw_assert(arena->ptr <= arena->end);
	return arena->ptr;
}
