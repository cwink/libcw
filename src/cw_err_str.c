#include "cw_assert.h"
#include "cw_err.h"

static const char *const cw_err_str_lookup[CW_ERR_MAX] = {
	[CW_ERR_NONE] = "no error has occured",
	[CW_ERR_SYS] = "a system error has occured",
};

const char *
cw_err_str(const enum cw_err err)
{
	cw_assert(err >= CW_ERR_NONE && err < CW_ERR_MAX);
	return cw_err_str_lookup[err];
}
