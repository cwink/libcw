#include <sys/mman.h>

#include "cw_arena.h"

enum cw_err
cw_arena_close(struct cw_arena *const arena)
{
	if (arena->open == CW_BOOL_TRUE && munmap(arena->ptr, (size_t)(arena->end - arena->beg)) != 0)
		return CW_ERR_SYS;
	arena->open = CW_BOOL_FALSE;
	return CW_ERR_NONE;
}
