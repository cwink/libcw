#include <sys/mman.h>

#include "cw_arena.h"
#include "cw_assert.h"

enum cw_err
cw_arena_open(struct cw_arena *const arena, const size_t size)
{
	cw_assert(arena->open == CW_BOOL_FALSE);
	if ((arena->ptr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_SHARED, -1, 0)) == MAP_FAILED)
		return CW_ERR_SYS;
	arena->beg = arena->ptr;
	arena->end = arena->beg + size;
	arena->open = CW_BOOL_TRUE;
	return CW_ERR_NONE;
}
